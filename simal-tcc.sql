-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Tempo de geração: 26/11/2019 às 12:33
-- Versão do servidor: 10.3.15-MariaDB-1:10.3.15+maria~xenial
-- Versão do PHP: 7.0.33-8+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `simal-tcc`
--

-- --------------------------------------------------------

--
-- Estrutura para tabela `barter`
--

CREATE TABLE `barter` (
  `id` int(11) NOT NULL,
  `book_id` int(11) NOT NULL,
  `link` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(45) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `books`
--

CREATE TABLE `books` (
  `title` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `author` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `genre_literary` int(11) NOT NULL,
  `opinion` text COLLATE utf8_unicode_ci NOT NULL,
  `type` tinyint(1) NOT NULL,
  `readed` tinyint(1) NOT NULL,
  `id` int(11) NOT NULL,
  `url` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ano` int(11) NOT NULL,
  `edicao` int(11) DEFAULT NULL,
  `id_user` int(11) NOT NULL,
  `lendable` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `books`
--

INSERT INTO `books` (`title`, `author`, `genre_literary`, `opinion`, `type`, `readed`, `id`, `url`, `ano`, `edicao`, `id_user`, `lendable`) VALUES
('outro livro', 'aaaa', 3, 'caralhooo', 0, 0, 1, '', 2001, 2, 1, 0),
('livro', 'gustavo', 2, 'aaaaaaaaaaaaaaaaaaaaaaaaaaa', 0, 0, 2, '', 2004, 2, 2, 1),
('ee', 'ee', 4, 'd', 1, 1, 3, '', 2222, 2, 1, 0),
('w', 'w', 1, 'w', 0, 0, 4, '', 2211, 5, 1, 0),
('know', 'know', 5, 'ffbb', 1, 1, 5, '', 44, 21, 2, 0),
('w', 'w', 1, 'w', 0, 0, 6, '', 2211, 6, 2, 0),
('mama', 'mama', 4, 'h', 1, 1, 7, '', 5555, 777, 1, 0),
('livro legal', 'we', 8, 'vv', 1, 1, 8, '', 4, 8, 2, 0),
('jjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj', 'I', 1, 'xx', 1, 0, 9, '', 4666, 88, 1, 0),
('zxxx', 'zxxx', 3, 'bb', 0, 0, 10, '', 9, 9, 1, 0),
('qwqw', 'qwqw', 9, 'xx', 1, 0, 11, '', 999, 8, 1, 0),
('kgkgkgk', 'kgkgkgk', 10, 'cc', 0, 0, 12, '', 7, 80, 1, 0),
('klob', 'blok', 3, 'ccvv', 0, 0, 13, '', 66, 45, 1, 0),
('blcul', 'blcul', 5, 'xxx', 1, 0, 14, '', 99, 8, 2, 0),
('a seleção', 'xi', 5, 'hnh', 1, 1, 15, '', 6, 6, 1, 1);

-- --------------------------------------------------------

--
-- Estrutura para tabela `genre_literary`
--

CREATE TABLE `genre_literary` (
  `id` int(11) NOT NULL,
  `categoria` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `valor` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `genre_literary`
--

INSERT INTO `genre_literary` (`id`, `categoria`, `valor`) VALUES
(1, 'Generalidades', 0),
(2, 'Filosofia', 100),
(3, 'Religião', 200),
(4, 'Ciências sociais', 300),
(5, 'Linguagem', 400),
(6, 'Ciências Naturais', 500),
(7, 'Ciências aplicadas', 600),
(8, 'Artes', 700),
(9, 'Literatura/teatro', 800),
(10, 'Geografia/História', 900);

-- --------------------------------------------------------

--
-- Estrutura para tabela `lists`
--

CREATE TABLE `lists` (
  `id` int(11) NOT NULL,
  `user_email` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(45) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `lists`
--

INSERT INTO `lists` (`id`, `user_email`, `name`) VALUES
(1, 'q', 'afazeres'),
(2, 'q', 'ssssss'),
(3, 'q', 'aa'),
(4, 'q', 'aasddfdfg'),
(5, 'q', 'fgdfgfhfg'),
(6, 'v@email.com', 'nova');

-- --------------------------------------------------------

--
-- Estrutura para tabela `options`
--

CREATE TABLE `options` (
  `id` int(11) NOT NULL,
  `list_id` int(11) NOT NULL,
  `option` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `conclued` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `options`
--

INSERT INTO `options` (`id`, `list_id`, `option`, `conclued`) VALUES
(1, 5, 'sdsd', 0),
(2, 6, 'e1', 0),
(3, 6, 'e2', 0);

-- --------------------------------------------------------

--
-- Estrutura para tabela `reading_list`
--

CREATE TABLE `reading_list` (
  `id` int(11) NOT NULL,
  `book_id` int(11) NOT NULL,
  `list_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `users`
--

CREATE TABLE `users` (
  `name` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `users`
--

INSERT INTO `users` (`name`, `email`, `password`, `id`) VALUES
('q', 'q', 'q', 1),
('vick', 'v@email.com', '123456', 2);

--
-- Índices de tabelas apagadas
--

--
-- Índices de tabela `books`
--
ALTER TABLE `books`
  ADD PRIMARY KEY (`id`),
  ADD KEY `genre_literary` (`genre_literary`);

--
-- Índices de tabela `genre_literary`
--
ALTER TABLE `genre_literary`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categoria` (`categoria`);

--
-- Índices de tabela `lists`
--
ALTER TABLE `lists`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `options`
--
ALTER TABLE `options`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de tabelas apagadas
--

--
-- AUTO_INCREMENT de tabela `books`
--
ALTER TABLE `books`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT de tabela `genre_literary`
--
ALTER TABLE `genre_literary`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT de tabela `lists`
--
ALTER TABLE `lists`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de tabela `options`
--
ALTER TABLE `options`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de tabela `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Restrições para dumps de tabelas
--

--
-- Restrições para tabelas `books`
--
ALTER TABLE `books`
  ADD CONSTRAINT `books_ibfk_1` FOREIGN KEY (`genre_literary`) REFERENCES `genre_literary` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
