<?php
	require_once "DAO/database.php";
	require_once "DAO/BookDAO.php";
	require_once "Class/Book.php";

	$con = conectar();


	$bookDao = new BookDAO($con);

	$dados = [];
	$resultado = [];

	$query = $bookDao->Listar();

	while ($registro = mysqli_fetch_assoc($query)) {
		$title = $registro['title'];
		$opinion = $registro['opinion'];
		$type = $registro['type'];
		$readed = $registro['readed'];
		$genre_literary = $registro['genre_literary'];
		$author = $registro['author'];
		$id = $registro['id'];
		$ano = $registro['ano'];
		$edicao = $registro['edicao'];

		$book = new Book($author, $title,$genre_literary,$opinion, $type, $readed,$ano, $edicao);

		$book->id=$id;
		$dados[] = $book;
	}

	$resultado = ['books' => $dados];

	desconectar($con);
	print json_encode($resultado);

?>
