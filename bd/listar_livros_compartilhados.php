<?php
  require_once "DAO/database.php";
  require_once "DAO/BookDAO.php";
  require_once "Class/Book.php";

  $con = conectar();


  $bookDao = new BookDAO($con);

  $resultado = [];
  $dados = [];

  $query = $bookDao->ListarBookShare();

  while ($registro = mysqli_fetch_assoc($query)) {
    $title = $registro['title'];
    $author = $registro['author'];
    $name = $registro['name'];
    $email = $registro['email'];

    array_push(
      $dados,
      (object)['title'=>$title,'author'=>$author,'name'=>$name,'email'=>$email]
    );
  }

  desconectar($con);
  print json_encode($dados);

?>
