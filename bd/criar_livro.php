<?php
	require_once "DAO/database.php";
	require_once "DAO/BookDAO.php";
	require_once "Class/Book.php";
	$result = ['result' => false];
	$con = conectar();
	$bookDao = new BookDAO($con);

	$author = "";
	$genre_literary = "";
	$opinion = "";
	$title = "";
	$url = "";
	$ano = "";
	$edicao = "";
	$id_user = "";
	$lendable = "";

	if (isset($_GET["title"])){
		$title = $_GET["title"];
		if (isset($_GET["author"])){
			$author = $_GET["author"];
			if (isset($_GET["opinion"])){
				$opinion = $_GET["opinion"];
				if (isset($_GET["genre_literary"])){
					$genre_literary = $_GET["genre_literary"];
					if(isset($_GET["type"])){
						$type = $_GET["type"];
						if(isset($_GET['readed'])){
							$readed = $_GET['readed'];
							if(isset($_GET['url'])){
								$url = $_GET['url'];
								if(isset($_GET['ano'])){
									$ano = $_GET['ano'];
									if(isset($_GET['edicao'])){
										$edicao = $_GET['edicao'];
										if(isset($_GET['id_user'])){
											$id_user = $_GET['id_user'];
											if(isset($_GET['lendable'])){
												$lendable = $_GET['lendable'];

												//http://localhost/aa/criar_livro.php?title=&author=&resume=&genre_literary=&url=
												$book = new Book($author, $title,$opinion,$genre_literary, $type, $readed,$url, $ano, $edicao,$id_user,$lendable);
												$bookDao->Inserir($book);

												$result = ['result' => true];
											}

										}
									}
								}
							}

						}
					}

				}
			}
		}
	}
	print json_encode($result);

	desconectar($con);

?>
