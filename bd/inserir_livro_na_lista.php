<?php
	require_once "DAO/database.php";
	require_once "DAO/ListaDAO.php";
	require_once "Class/Lista.php";
	$result = ['result' => false];

	$con = conectar();
	$listaDao = new ListaDAO($con);
	$id_lista = "";
	$id_book = "";

	if (isset($_GET["id_lista"])){
		$id_lista = $_GET["id_lista"];
    		if(isset($_GET["id_book"])){
			$id_book = $_GET["id_book"];
	  		//http://localhost/aa/inserir_livro_na_lista.php?id_lista=&id_book=

			$listaDao->InserirLivro($id_lista, $id_book);
			$result = ['result' => true];
		}
	}

	desconectar($con);
	print json_encode($result);

?>
