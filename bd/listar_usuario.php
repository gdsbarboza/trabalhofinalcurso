<?php
	require_once "DAO/database.php";
	require_once "DAO/UserDAO.php";
	require_once "Class/User.php";
	$con = conectar();


	$userDao = new UserDAO($con);

	$dados = [];

	$query = $userDao->Listar();

	while ($registro = mysqli_fetch_assoc($query)) {
		$name = $registro['name'];
		$email = $registro['email'];
		$password = $registro['password'];
		$user = new User($email, $name, $password);
		$dados[] = ['user' => $user];
	}

	print json_encode($dados);
	desconectar($con);
?>
