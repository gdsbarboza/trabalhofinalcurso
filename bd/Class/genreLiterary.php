<?php 

	class genreLiterary{
		public $id;
		public $categoria;
		public $valor;

		public function genreLiterary($categoria, $valor){
			$this->categoria = $categoria;
			$this->valor = $valor;
		}

		public function getCategoria(){
			return $this->categoria;
		}

		public function getValor(){
			return $this->valor;
		}

		public function getId(){
			return $this->id;
		}

		public function setCategoria($categoria){
			$this->categoria = $categoria;
		}

		public function setValor($valor){
			$this->valor = $valor;
		}

		public function setId($id){
			$this->id = $id;
		}
	}

 ?>