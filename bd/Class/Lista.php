<?php
class Lista{
	public $id;
	public $name;
	public $email_user;

	public function Lista($name, $email_user){
		$this->name = $name;
		$this->email_user = $email_user;
	}

	public function getId(){
		return $this->id;
	}

	public function getName(){
		return $this->name;

	}

	public function setName($valor){
		$this->name = $valor;
	}

	public function getEmail_user(){
		return $this->email_user;
	}

	public function setEmail_user($email_user){
		return $this->email_user = $email_user;
	}
}
 ?>
