<?php
class readingList{
	public $id;
	public $list_id;
	public $book_id;

	public function readingList($list_id, $book_id){
		$this->list_id = $list_id;
		$this->book_id = $book_id;
	}

	public function getId(){
		return $this->id;
	}

	public function getList_id(){
		return $this->list_id;

	}

	public function setList_id($valor){
		$this->list_id = $valor;
	}

	public function getBook_id(){
		return $this->book_id;
	}

	public function setBook_id($book_id){
		return $this->book_id = $book_id;
	}
}
 ?>
