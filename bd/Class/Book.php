<?php
class Book{
	public $author;
	public $title;
	public $genre_literary;
	public $opinion;
	public $type;
	public $readed;
	public $id;
	public $url;
	public $ano;
	public $edicao;
	public $id_user;
	public $lendable;

	public function Book($author, $title,$opinion,$genre_literary, $type, $readed, $url, $ano, $edicao, $id_user, $lendable){
		$this->author = $author;
		$this->title = $title;
		$this->genre_literary = $genre_literary;
		$this->opinion = $opinion;
		$this->type = $type;
		$this->readed = $readed;
		$this->url = $url;
		$this->ano = $ano;
		$this->edicao = $edicao;
		$this->id_user = $id_user;
		$this->lendable = $lendable;
	}

	public function setLendable($lendable){
		$this->lendable = $lendable;
	}

	public function isLendable(){
		return $this->lendable;
	}

	public function setId($id){
		$this->id = $id;
	}

	public function getId(){
		return $this->id;
	}

	public function getUrl(){
		return $this->url;
	}

	public function getTitle(){
		return $this->title;

	}

	public function getIdUser(){
		return $this->id_user;

	}

	public function getGenre_literary(){
		return $this->genre_literary;
	}

	public function getAuthor(){
		return $this->author;

	}

	public function getOpinion(){
		return $this->opinion;

	}

	public function getType(){
		return $this->type;

	}

	public function isReaded(){
		return $this->readed;

	}

	public function getAno(){
		return $this->ano;
	}

	public function getEdicao(){
		return $this->edicao;
	}

	public function setAno($ano){
		$this->ano = $ano;
	}

	public function setEdicao($edicao){
		$this->edicao = $edicao;
	}

	public function setTitle($valor){
		$this->title = $valor;
	}

	public function setGenre_literary($genre_literary, $valor){
		return $this->genre_literary = $valor;
	}


	public function setAuthor($valor){
		$this->author = $valor;
	}


	public function setOpinion($valor){
		$this->opinion = $valor;
	}

	public function setType($valor){
		$this->type = $valor;
	}

	public function setReaded($valor){
		$this->readed = $valor;
	}
}
 ?>
