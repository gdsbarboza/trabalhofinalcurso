<?php

	class Options{
		public $id;
		public $list_id;
		public $option;
		public $conclued;

		public function Options($option, $list_id, $conclued){
			$this->option = $option;
			$this->list_id = $list_id;
			$this->conclued = $conclued;
		}

		public function getId(){
			return $this->id;
		}
		public function setId($id){
			$this->id = $id;
		}

		public function getIdLista(){
			return $this->list_id;
		}

		public function setIdLista($list_id){
			 $this->list_id = $list_id;
		}

		public function getOption(){
			return $this->option;
		}

		public function setOption($option){
			$this->option = $option;
		}

		public function getConclued(){
			return $this->conclued;
		}

		public function setConclued($conclued){
			 $this->conclued = $conclued;
		}

	}



 ?>
