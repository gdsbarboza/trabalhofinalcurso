<?php
class Lists{
	public $id;
	public $name;
	public $user_email;

	public function Lists($name, $user_email){
		$this->name = $name;
		$this->user_email = $user_email;
	}

	public function getId(){
		return $this->id;
	}

	public function getName(){
		return $this->name;
	}

	public function setName($valor){
		$this->name = $valor;
	}

	public function getUser_email(){
		return $this->user_email;
	}

	public function setUser_email($user_email){
		return $this->user_email = $user_email;
	}
}
 ?>
