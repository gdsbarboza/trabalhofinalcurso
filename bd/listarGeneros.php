<?php
	require_once "DAO/database.php";
	require_once "Class/genreLiterary.php";
	require_once "DAO/GenreLiteraryDAO.php";

	$con = conectar();

	$genreDao = new GenreLiteraryDAO($con);

	$dados = [];
	$resultado = [];

	$query = $genreDao->Listar();

	while ($registro = mysqli_fetch_assoc($query)) {
		$categoria = $registro['categoria'];
		$valor = $registro['valor'];

		$genre = new genreLiterary($categoria, $valor);

		$genre->id= $registro['id'];
		$dados[] = $genre;
	}

	$resultado = ['Literary genre' => $dados];

	desconectar($con);
	print json_encode($resultado);
?>
