<?php
	require_once "DAO/database.php";
	require_once "DAO/ListaDAO.php";
	require_once "Class/Lista.php";

	$con = conectar();
	$listaDao = new ListaDAO($con);
	$string = "";
	$dados = [];
	
	if (isset($_GET["string"])){
		$string = $_GET["string"];

		//http://localhost/aa/buscar_listas.php?string=

		$dados = [];

		$query = $listaDao->Buscar($string);

		while ($registro = mysqli_fetch_assoc($query)) {
			$name = $registro['name'];
			$email_user = $registro['email_user'];
			$id = $registro['id'];
			$lista = new Lista($name, $email_user);
			$lista->id=$id;
			$dados[] = ['lista' => $lista];
		}



	}
	desconectar($con);
	print json_encode($dados);

?>
