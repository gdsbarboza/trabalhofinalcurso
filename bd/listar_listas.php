<?php
	require_once "DAO/database.php";
	require_once "DAO/ListaDAO.php";
	require_once "Class/Lista.php";
	$con = conectar();


	$listaDao = new ListaDAO($con);
	$resultado;
	$email = "";

	$dados = [];

	if (isset($_GET["email_user"])){
		$email = $_GET["email_user"];
	}

	$query = $listaDao->Listar($email);

	while ($registro = mysqli_fetch_assoc($query)) {

		$name = $registro['name'];
		$email_user = $registro['user_email'];
		$id = $registro['id'];
		$lista = new Lista($name,$email_user);
   		$lista->id=$id;
		$dados[] = $lista;
	}
		$resultado = ['listas' => $dados];
	print json_encode($resultado);
	desconectar($con);
?>
