<?php

	function conectar() {
		$servidor = "localhost";
		$usuario = "root";
		$senha = "root";
		$bd = "simal-tcc";

		$con = mysqli_connect($servidor, $usuario, $senha);
		mysqli_select_db($con, $bd);
		mysqli_set_charset($con, "utf8");

		return $con;
	}

	function desconectar($con){
		mysqli_close($con);
	}
 ?>
