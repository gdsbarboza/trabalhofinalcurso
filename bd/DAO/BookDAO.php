<?php
include_once('/var/www/html/bd/Class/Book.php');
//include_once('C:\xampp\htdocs\bd\Class\Book.php');

class BookDAO{
	private $conexao;

	public function BookDAO($conexao){
		$this->conexao = $conexao;
	}

	public function Listar(){
		$sql = "select * from books;";
		return mysqli_query($this->conexao, $sql);
	}

	public function ListarPorIdUser($id){
		$sql = "select * from books where id_user = {$id} and type is false;";
		return mysqli_query($this->conexao, $sql);
	}

	public function listarTrocaveis(){
		$sql = "select * from books where lendable = 1;";
		return mysqli_query($this->conexao, $sql);
	}

	public function ListarFisicos(){
		$sql = "select * from books where type is false;";
		return mysqli_query($this->conexao, $sql);
	}

	public function ListarDigitais(){
		$sql = "select * from books where type is true;";
		return mysqli_query($this->conexao, $sql);
	}

	public function ListarBookId($id){
		$sql = "select * from books where id={$id};";
		return mysqli_query($this->conexao, $sql);
	}

	public function ListarOrganizado($id){
		$sql = "select * from books where (id_user = {$id} and type is false) order by genre_literary,title,edicao;";
		return mysqli_query($this->conexao, $sql);
	}

//tem que revisar esse codigo pois genre literary é uma classe agora
	public function Buscar($string){
		$sql = "select * from books where author like '%{$string}%' OR title like '%{$string}%' OR genre_literary like '%{$string}%';";
		return mysqli_query($this->conexao, $sql);
	}

	public function Inserir(Book $book){
		$sql = "insert into books (title, author, opinion, genre_literary,type, readed, url, ano, edicao,id_user,lendable) values ('{$book->getTitle()}','{$book->getAuthor()}','{$book->getOpinion()}',{$book->getGenre_literary()},{$book->getType()},{$book->isReaded()},'{$book->getUrl()}',{$book->getAno()}, {$book->getEdicao()}, {$book->getIdUser()}, {$book->isLendable()} );";
		mysqli_query($this->conexao, $sql);
	}

	public function update(Book $book){
		$sql = "update books set title = '{$book->getTitle()}', author = '{$book->getAuthor()}', genre_literary= {$book->getGenre_literary()}, opinion = '{$book->getOpinion()}', type = {$book->getType()}, readed = {$book->isReaded()}, url = '{$book->getUrl()}', ano = {$book->getAno()}, edicao = {$book->getEdicao()}, lendable = {$book->isLendable} where id = {$book->getId()};";
		mysqli_query($this->conexao, $sql);
	}

	public function ListarBookShare(){
		$sql = "select b.title, b.author, u.name, u.email from books b, users u where b.lendable = true and u.id = b.id_user and b.type = false;";
		return mysqli_query($this->conexao, $sql);
	}

	public function Deletar($id){
		$sql = "delete from books where id={$id};";
		mysqli_query($this->conexao, $sql);
	}
}
 ?>
