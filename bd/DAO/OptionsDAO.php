<?php
include_once('/var/www/html/bd/Class/Options.php');
//include_once('C:\xampp\htdocs\bd\Class\Options.php');

class OptionsDAO{
	private $conexao;

	public function OptionsDAO($conexao){
		$this->conexao = $conexao;
	}

	public function Listar(){
		$sql = "select * from options;";
		return mysqli_query($this->conexao, $sql);
	}

	public function update(Options $option){
		$sql = "update options set list_id = {$option->getIdLista()}, options.option ='{$option->getOption()}',conclued = {$option->getConclued()} where id = {$option->getId()};";
		mysqli_query($this->conexao, $sql);
	}

	public function ListarOptionsIdLista($id){
		$sql = "select option from options where list_id={$id};";
		return mysqli_query($this->conexao, $sql);
	}

	public function Inserir(Options $option){
		$sql = "insert into options (list_id, option, conclued) values ({$option->getIdLista()},'{$option->getOption()}','{$option->getConclued()}');";
		mysqli_query($this->conexao, $sql);
	}

	public function Deletar($id){
		$sql = "delete from options where list_id={$id};";
		mysqli_query($this->conexao, $sql);
	}
}
 ?>
