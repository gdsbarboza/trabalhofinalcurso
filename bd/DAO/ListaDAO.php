<?php
include_once('/var/www/html/bd/Class/Lista.php');
//include_once('C:\xampp\htdocs\bd\Class\Lista.php');
class ListaDAO{
	private $conexao;

	public function ListaDAO($conexao){
		$this->conexao = $conexao;
	}

	public function Listar($string){
		$sql = "select * from lists where user_email = '{$string}'";
		return mysqli_query($this->conexao, $sql);
	}

	public function ListarLivrosPorLista($id_lista){
		$sql = "select b.title as title from book_list bl, books b where b.id=bl.id_book and bl.id_list={$id_lista};";
		return mysqli_query($this->conexao, $sql);
	}

	public function ListarListaId($id){
		$sql = "select * from lists where id={$id};";
		return mysqli_query($this->conexao, $sql);
	}

	public function Buscar($string){
		$sql = "select * from lists where name like '{$string}';";
		return mysqli_query($this->conexao, $sql);
	}

	public function Inserir(Lista $lista){
		$sql = "insert into lists (name,user_email) values ('{$lista->getName()}','{$lista->getEmail_user()}');";
		mysqli_query($this->conexao, $sql);
		//$last_id = mysqli_insert_id($this->conexao);
	}

	public function InserirLivro($id_lista, $id_book){
    $sql = "insert into book_list (id_list,id_book) values ({$id_lista},{$id_book})";
		mysqli_query($this->conexao, $sql);
	}

	public function Deletar(Lista $lista){
		$sql = "delete from lists where name='{$lista->getName()}' AND user_email='{$lista->getEmail_user()}';";
		mysqli_query($this->conexao, $sql);
	}

}
 ?>
