<?php
	require_once "DAO/database.php";
	require_once "DAO/BookDAO.php";
	require_once "Class/Book.php";

	$con = conectar();
	$bookDao = new BookDAO($con);
	$string = "";

	if (isset($_GET["id"])){
		$string = $_GET["id"];

		//http://localhost/aa/buscar_livro.php?string=

		$dados = [];
		$resultado = [];
		$query = $bookDao->ListarBookId($string);

		while ($registro = mysqli_fetch_assoc($query)) {
			$title = $registro['title'];
			$opinion = $registro['opinion'];
			$type = $registro['type'];
			$readed = $registro['readed'];
			$genre_literary = $registro['genre_literary'];
			$author = $registro['author'];
			$id = $registro['id'];
			$ano = $registro['ano'];
			$edicao = $registro['edicao'];
			$id_user = $registro['id_user'];
			$url = $registro['url'];
			$lendable = $registro['lendable'];

			$book = new Book($author, $title,$opinion,$genre_literary, $type, $readed,$url,$ano, $edicao, $id_user,$lendable);
			$book->id=$id;
			$dados[] = $book;
		}

		$resultado = ['books' => $dados];

	}
	desconectar($con);
	print json_encode($resultado);
