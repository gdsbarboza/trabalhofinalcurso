<?php
	require_once "DAO/database.php";
	require_once "DAO/OptionsDAO.php";
	require_once "Class/Options.php";

	$result = ['result' => false];
	$con = conectar();
	$itemDao = new OptionsDAO($con);

	$item = "";
	$id = "";
	$list_id = "";
	$option ="";
	$conclued = "";

	if (isset($_GET["list_id"])){
		$list_id = $_GET["list_id"];
		if (isset($_GET["option"])){
			$option = $_GET["option"];
			if (isset($_GET["conclued"])){
				$conclued = $_GET["conclued"];
				if(isset($_GET["id"])){	
					$id = $_GET["id"];
					//http://localhost/aa/criar_livro.php?list_id=&option=&conlued&id
					
					$item = new Options($option, $list_id,$conclued);
					$item->setId($id);

					$itemDao->update($item);

					$result = ['result' => true];
				}			
			}
		}
	}

	print json_encode($result);

?>
