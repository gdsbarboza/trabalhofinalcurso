<?php
	require_once "DAO/database.php";
	require_once "DAO/OptionsDAO.php";
	require_once "Class/Options.php";

	$con = conectar();
	$optionsDao = new OptionsDAO($con);
	$list_id = "";
	$dados = [];
	if (isset($_GET["list_id"])){
		$list_id = $_GET["list_id"];

		//http://localhost/aa/listar_options_por_lista.php?list_id=



		$query = $optionsDao->ListarOptionsIdLista($list_id);

		while ($registro = mysqli_fetch_assoc($query)) {
			$option = $registro['option'];

			$dados[] = $option;
		}

	}
	print json_encode($dados);
	desconectar($con);


?>
