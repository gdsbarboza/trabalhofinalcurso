<?php
	require_once "DAO/database.php";
	require_once "DAO/ListaDAO.php";
	require_once "Class/Lista.php";

	$con = conectar();
	$listaDao = new ListaDAO($con);
	$id_lista = "";

	if (isset($_GET["id_lista"])){
		$id_lista = $_GET["id_lista"];

		//http://localhost/aa/listar_livro_por_lista.php?id_lista=

		$dados = [];

		$query = $listaDao->ListarLivrosPorLista($id_lista);

		while ($registro = mysqli_fetch_assoc($query)) {
			$title = $registro['title'];

			$dados[] = ['books' => $title];
		}

	}
	desconectar($con);
	print json_encode($dados);

?>
