<?php
	require_once "DAO/database.php";
	require_once "DAO/OptionsDAO.php";
	require_once "Class/Options.php";
	$result = ['result' => false];
	$con = conectar();
	$optionsDao = new OptionsDAO($con);
	$list_id = "";

	if (isset($_GET["list_id"])){
		$list_id = $_GET["list_id"];
		//http://localhost/aa/deletar_options_por_lista.php?list_id=
		$optionsDao->Deletar($list_id);
		$result = ['result' => true];
	}
	desconectar($con);
	print json_encode($result);

?>
