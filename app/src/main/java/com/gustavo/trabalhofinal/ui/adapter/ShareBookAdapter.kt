package com.gustavo.trabalhofinal.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.gustavo.trabalhofinal.R
import com.gustavo.trabalhofinal.entities.RetornoShared
import kotlinx.android.synthetic.main.card_book_share.view.*

class ShareBookAdapter(var books: List<RetornoShared>) : RecyclerView.Adapter<ShareBookAdapter.CategoriaViewHolder>() {

    override fun getItemCount() = books.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
            CategoriaViewHolder(
                    LayoutInflater
                            .from(parent.context)
                            .inflate(R.layout.card_book_share, parent, false)
            )

    override fun onBindViewHolder(holder: CategoriaViewHolder, position: Int) {
        holder.fillUI(books[position])
    }


    inner class CategoriaViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun fillUI(book: RetornoShared) {


            itemView.txtTitle.text = book.title
            itemView.txtAuthor.text = book.author
            itemView.txtName.text = book.name
            itemView.txtEmail.text = book.email
        }


    }

}
