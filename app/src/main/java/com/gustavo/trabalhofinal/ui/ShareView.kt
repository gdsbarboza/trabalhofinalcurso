package com.gustavo.trabalhofinal.ui

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.gustavo.trabalhofinal.R
import com.gustavo.trabalhofinal.entities.Book
import com.gustavo.trabalhofinal.entities.RetornoShared
import com.gustavo.trabalhofinal.service.BancoService
import com.gustavo.trabalhofinal.ui.adapter.ShareBookAdapter
import kotlinx.android.synthetic.main.fragment_share_view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class ShareView : androidx.fragment.app.Fragment(){
    lateinit var retrofit: Retrofit
    lateinit var service: BancoService
    lateinit var adapter: ShareBookAdapter
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_share_view, container,false)
    }
    fun configuraRetrofit() {
        retrofit = Retrofit.Builder()
                .baseUrl("http://10.0.2.2/bd/")
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        service = retrofit.create(BancoService::class.java)
    }
    fun configuraRecyclerView(livros: List<RetornoShared>) {



        adapter = ShareBookAdapter(livros)

       listBookShared.adapter = adapter

        listBookShared.layoutManager = LinearLayoutManager(getView()!!.context, RecyclerView.VERTICAL, false)
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        configuraRetrofit()
        service.list_shared_books().enqueue(object : Callback<List<RetornoShared>> {
            override fun onFailure(call: Call<List<RetornoShared>>, t: Throwable) {
                Log.d("vick", t.message, t)
            }

            override fun onResponse(call: Call<List<RetornoShared>>, response: Response<List<RetornoShared>>) {

                val r = response.body()!!

                configuraRecyclerView(r)
            }
        })

    }

}
