package com.gustavo.trabalhofinal.ui


import android.app.Activity.RESULT_OK
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Spinner
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.gustavo.trabalhofinal.R
import com.gustavo.trabalhofinal.entities.ReturnBooks
import com.gustavo.trabalhofinal.entities.ReturnCreate
import com.gustavo.trabalhofinal.service.BancoService
import kotlinx.android.synthetic.main.fragment_add_book.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class AddBookFragment : Fragment() {
    lateinit var retrofit: Retrofit
    lateinit var service: BancoService
    lateinit var uri: String
    lateinit var prefs: SharedPreferences
    var flag: Int = 0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment


        return inflater.inflate(R.layout.fragment_add_book, container, false)
    }

    fun configuraRetrofit() {
        retrofit = Retrofit.Builder()
                .baseUrl("http://10.0.2.2/bd/")
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        service = retrofit.create(BancoService::class.java)
    }

    override fun onActivityResult(codigo: Int, resultado: Int, data: Intent?) {
        if (codigo == 1 && resultado == RESULT_OK) {
            uri = data?.data.toString()
            bookImg.setImageURI(data?.data)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        configuraRetrofit()


        var list_of_items = arrayOf("","Generalidades", "Filosofia", "Religião","Ciências sociais","Linguagem","Ciências Naturais", "Ciências aplicadas","Artes", "Literatura/teatro", "Geografia/História")
        //aqui populamos o spinner <3 s2

        literaryGenre!!.setOnItemSelectedListener(this)
        // Create an ArrayAdapter using a simple spinner layout and languages array
        val aa = ArrayAdapter(activity                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  , android.R.layout.simple_spinner_item , list_of_items)
        // Set layout to use when the list of choices appear
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        // Set Adapter to Spinner
        literaryGenre!!.setAdapter(aa)

        prefs = this.activity!!.getSharedPreferences("prefs", Context.MODE_PRIVATE)

        if(prefs.contains("id_book") && prefs.getInt("id_book", 0) != 0){
        var teste = prefs.getInt("id_book", 0)
            Log.d("vick", teste.toString())
            service.searchBookId(prefs.getInt("id_book", 0)).enqueue(object : Callback<ReturnBooks> {
                override fun onFailure(call: Call<ReturnBooks>, t: Throwable) {
                }

                override fun onResponse(call: Call<ReturnBooks>, response: Response<ReturnBooks>) {


                    if (response.body()!= null) {
                        val r = response.body()!!

                        bookTitle.setText(r.books.last().title)
                        bookAutor.setText(r.books.last().author)
                        literaryGenre.setSelection(r.books.last().genre)
                        isReaded.isChecked = r.books.last().readed
                        DigitalVerification.isSelected = r.books.last().type
                        resume.setText(r.books.last().resume)
                        bookedicao.setText(r.books.last().edicao.toString())
                        bookdate.setText(r.books.last().ano.toString())
                        lendable.isChecked = r.books.last().lendable

                        flag = 1
                    }
                }
            })

        }

        uri = ""
        bookImg.setOnClickListener {
            val i = Intent(Intent.ACTION_GET_CONTENT)
            i.type = "image/*"
            startActivityForResult(Intent.createChooser(i, "Selecione uma imagem"), 1)
        }

        book_submit.setOnClickListener {

            var title = bookTitle.text.toString()
            var autor = bookAutor.text.toString()
            var genre = literaryGenre.selectedItemPosition
            var readed = isReaded.isChecked()
            var digital = DigitalVerification.isChecked(    )
            var opinion = resume.text.toString()
            var ano = bookdate.text.toString().toInt()  //mudar isso
            var edicao = bookedicao.text.toString().toInt()  // mudar isso
            var lendable = lendable.isChecked()

            if(flag == 0) {
                if (title != "" && autor != "" && genre != null && opinion != "" && ano != null && genre != 0) {
                    service.createBook(title, autor, opinion, genre, digital, readed, uri, ano , edicao, prefs.getInt("id_user",0), lendable).enqueue(object : Callback<ReturnCreate> {
                        override fun onFailure(call: Call<ReturnCreate>, t: Throwable) {
                            Log.w("vick", t.message, t)
                        }

                        override fun onResponse(call: Call<ReturnCreate>, response: Response<ReturnCreate>) {
                            val r = response.body()!!

                            Log.w("vick", r.result.toString())
                            if (r.result) {
                                flag = 0
                                findNavController().navigate(R.id.menu)

                            }
                        }

                    })

                } else {
                    bookTitle.setText("")
                    bookAutor.setText("")
                    literaryGenre.setSelection(0)
                    isReaded.setSelected(false)
                    DigitalVerification.setSelected(false)
                    bookedicao.setText("")
                    bookdate.setText("")
                    resume.setText("")
                }
            }else{
                var idMeuLivro = prefs.getInt("id_book", 0)
                if (title != "" && autor != "" && genre != null && opinion != "") {
                    service.updateBook(idMeuLivro,title, autor, opinion, genre, digital, readed, uri, ano,edicao,prefs.getInt("id_user", 0),lendable ).enqueue(object : Callback<ReturnCreate> {
                        override fun onFailure(call: Call<ReturnCreate>, t: Throwable) {
                            Log.w("vick", t.message, t)
                        }

                        override fun onResponse(call: Call<ReturnCreate>, response: Response<ReturnCreate>) {
                            flag = 0
                            prefs.edit().putInt("id_book", 0).commit()
                            findNavController().navigate(R.id.menu)
                        }

                    })

                } else {
                    bookTitle.setText("")
                    bookAutor.setText("")
                    literaryGenre.setSelection(0)
                    isReaded.setSelected(false)
                    DigitalVerification.setSelected(false)
                    resume.setText("")
                    bookedicao.setText("")
                    bookdate.setText("")
                    this.lendable.setSelected(false)
                }
            }

        }
    }

}

private fun Spinner.setOnItemSelectedListener(                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          addBookFragment: AddBookFragment) {

}
