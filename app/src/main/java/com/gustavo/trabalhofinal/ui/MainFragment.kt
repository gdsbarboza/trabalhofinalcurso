package com.gustavo.trabalhofinal.ui


import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.NavigationUI
import androidx.navigation.ui.setupWithNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.gustavo.trabalhofinal.R
import com.gustavo.trabalhofinal.entities.Book
import com.gustavo.trabalhofinal.entities.ReturnBooks
import com.gustavo.trabalhofinal.entities.ReturnCreate
import com.gustavo.trabalhofinal.service.BancoService
import com.gustavo.trabalhofinal.ui.adapter.BookAdapter
import com.gustavo.trabalhofinal.ui.adapter.BookListener
import kotlinx.android.synthetic.main.activity_main2.view.*
import kotlinx.android.synthetic.main.fragment_login.view.*
import kotlinx.android.synthetic.main.fragment_main.*
import kotlinx.android.synthetic.main.nav_header_main2.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class MainFragment : androidx.fragment.app.Fragment(), BookListener {


	lateinit var retrofit: Retrofit
	lateinit var service: BancoService
	lateinit var adapter: BookAdapter
	lateinit var prefs: SharedPreferences

	override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,savedInstanceState: Bundle?): View? {
		// Inflate the layout for this fragment
		return inflater.inflate(R.layout.fragment_main, container, false)
	}
	fun configuraRetrofit() {
		retrofit = Retrofit.Builder()
				.baseUrl("http://10.0.2.2/bd/")
				.addConverterFactory(GsonConverterFactory.create())
				.build()
		service = retrofit.create(BancoService::class.java)
	}

	private fun loadRecyclerView(books: List<Book>) {

		if (bookList != null) {

			val mRecycler = getView()!!.findViewById(R.id.bookList) as RecyclerView
			mRecycler.layoutManager = LinearLayoutManager(getView()!!.context, RecyclerView.VERTICAL,false)
			adapter = BookAdapter(books, this, prefs.getInt("id_user", 0
			))
			mRecycler.adapter = adapter

		}
	}

	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		super.onViewCreated(view, savedInstanceState)
		configuraRetrofit()

		prefs = this.context!!.getSharedPreferences("prefs", Context.MODE_PRIVATE)

		if(prefs.contains("id_book")){
			prefs.edit().putInt("id_book", 0).commit()
		}
		service.searchIdUser(prefs.getString("email",""),prefs.getString("senha","")).enqueue(object : Callback<Int> {
			override fun onFailure(call: Call<Int>, t: Throwable) {
				Log.e("vick", t.message, t)
			}

			@SuppressLint("ApplySharedPref")
			override fun onResponse(call: Call<Int>, response: Response<Int>) {

				prefs.edit().putInt("id_user", response.body()!!.toInt()).commit()

			}

		})

		//activity.get.setText(prefs.getString("name", "user"))
		//view.nav_header_subtitle.setText(prefs.getString("email", "email@email.com"))

		botton_navgint.setupWithNavController(
				Navigation.findNavController(view)
		)

		botton_navgint.setOnNavigationItemSelectedListener {
			NavigationUI.onNavDestinationSelected(it,
					Navigation.findNavController(view))
		}


		if(prefs.getBoolean("nav",false) == false) {
			service.listOwnBooks(prefs.getInt("id_user",0)).enqueue(object : Callback<ReturnBooks> {
				override fun onFailure(call: Call<ReturnBooks>, t: Throwable) {
					Log.d("vick", t.message, t)
				}

				override fun onResponse(call: Call<ReturnBooks>, response: Response<ReturnBooks>) {
					if(response.body()!= null) {
						loadRecyclerView(response.body()!!.books)
					}
				}
			})
		}

		if(prefs.getBoolean("nav",false) == true) {
			service.listDigitalBook().enqueue(object : Callback<ReturnBooks> {
				override fun onFailure(call: Call<ReturnBooks>, t: Throwable) {
					Log.d("vick", t.message, t)
				}

				override fun onResponse(call: Call<ReturnBooks>, response: Response<ReturnBooks>) {

					if(response.body()!= null) {
						loadRecyclerView(response.body()!!.books)
					}
				}
			})
		}

	}
	override fun editarBook(book: Book) {
		prefs.edit().putInt("id_book", book.id).commit()

		findNavController().navigate(R.id.add_book)

	}

	override fun excluirBook(book: Book) {
		service.deleteBook(book.id).enqueue(object : Callback<ReturnCreate> {
			override fun onFailure(call: Call<ReturnCreate>, t: Throwable) {
				Log.d("vick", t.message, t)
			}

			override fun onResponse(call: Call<ReturnCreate>, response: Response<ReturnCreate>) {

				val r = response.body()!!
				if (r.result)
					findNavController().navigate(R.id.menu)
			}
		})
	}
}
