package com.gustavo.trabalhofinal.ui


import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.NavigationUI
import androidx.navigation.ui.setupWithNavController
import com.gustavo.trabalhofinal.R
import com.gustavo.trabalhofinal.entities.User
import com.gustavo.trabalhofinal.service.BancoService
import kotlinx.android.synthetic.main.fragment_login.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


/**
 * A simple [Fragment] subclass.
 * Use the [LoginFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class LoginFragment: androidx.fragment.app.Fragment(){

    lateinit var retrofit: Retrofit
    lateinit var service: BancoService
    lateinit var prefs: SharedPreferences

    override fun onCreateView( inflater: LayoutInflater, container: ViewGroup?,  savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.fragment_login, container, false)
    }
    fun configuraRetrofit() {

        retrofit = Retrofit.Builder()
                .baseUrl("http://10.0.2.2/bd/")
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        service = retrofit.create(BancoService::class.java)
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        prefs = this.activity!!.getSharedPreferences("prefs", Context.MODE_PRIVATE)

        if(prefs.contains("id_user")){
            findNavController().navigate(R.id.menu)
        }

        btMenu.setOnClickListener {

            var email = txtEmail.text.toString()
            var password = txtPassword.text.toString()
            configuraRetrofit()
            service.loginUser(email, password).enqueue(object : Callback<User> {
                override fun onFailure(call: Call<User>, t: Throwable) {
                    Log.e("vick", t.message, t)
                }

                override fun onResponse(call: Call<User>, response: Response<User>) {
                    if(response.body() != null) {
                        prefs.edit().putString("email", email).commit()
                        prefs.edit().putString("name", response.body()!!.name).commit()
                        prefs.edit().putString("senha", password).commit()
                        findNavController().navigate(R.id.menu)
                    }else{
                        txtPassword.setText("")
                        txtEmail.setText("")
                    }
                }

            })

        }
        //btMenu.setOnClickListener (Navigation.createNavigateOnClickListener(R.id.menu))
    }
}