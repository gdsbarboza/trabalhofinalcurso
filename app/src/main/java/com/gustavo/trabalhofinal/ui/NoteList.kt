package com.gustavo.trabalhofinal.ui

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.gustavo.trabalhofinal.R
import com.gustavo.trabalhofinal.entities.Option
import com.gustavo.trabalhofinal.entities.ReturnCreate
import com.gustavo.trabalhofinal.service.BancoService
import com.gustavo.trabalhofinal.ui.adapter.SubitenListaAdapter
import kotlinx.android.synthetic.main.fragment_note_list.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class NoteList : Fragment() {
    lateinit var retrofit: Retrofit
    lateinit var service: BancoService
    lateinit var prefs: SharedPreferences
    lateinit var adapter: SubitenListaAdapter
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.fragment_note_list, container, false)
    }
    fun configuraRetrofit() {
        retrofit = Retrofit.Builder()
                .baseUrl("http://10.0.2.2/bd/")
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        service = retrofit.create(BancoService::class.java)
    }
    fun configuraRecyclerView(options: List<Option>) {



        adapter = SubitenListaAdapter(options.toMutableList())

        mrvSubItemList.adapter = adapter

        mrvSubItemList.layoutManager = LinearLayoutManager(
                getView()!!.context, RecyclerView.VERTICAL, false)
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        configuraRetrofit()
        prefs = this.context!!.getSharedPreferences("prefs", Context.MODE_PRIVATE)
        val userEmail = prefs.getString("email","")
        val listTitulo = prefs.getString("listaTitle","")
        val listId = prefs.getInt("listaId",1)
        var options = mutableListOf<Option>()
        txtNameLista.text = listTitulo

        service.listOptions(listId).enqueue(object : Callback<List<String>> {
            override fun onFailure(call: Call<List<String>>, t: Throwable) {
                Log.d("vick", t.message, t)
            }

            override fun onResponse(call: Call<List<String>>, response: Response<List<String>>) {

                val r = response.body()!!

                if (r.size!=0 ) {
                    for (i in 0..r.size-1) {

                        var option = Option(listId, r[i])
                            options.add(option)
                    }
                    configuraRecyclerView(options)
                }
            }
        })
        btSalvar.setOnClickListener {
            if(txtNote.text.toString() != null){
                var option = Option(listId, txtNote.text.toString())

                service.createOptions(option.list_id,option.option,false).enqueue(object : Callback<ReturnCreate> {
                    override fun onFailure(call: Call<ReturnCreate>, t: Throwable) {
                        Log.d("vick", t.message, t)
                    }

                    override fun onResponse(call: Call<ReturnCreate>, response: Response<ReturnCreate>) {

                        val r = response.body()!!
                        if (r.result){
                            txtNote.setText("")
                            options.add(option)
                            configuraRecyclerView(options)
                        }

                    }
                });
            }
        }
        btApagar.setOnClickListener {
            findNavController().navigate(R.id.action_noteList_to_listAddFragment)
        }

    }
}
