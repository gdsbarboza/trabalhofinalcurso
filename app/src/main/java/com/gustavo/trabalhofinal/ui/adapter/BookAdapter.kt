package com.gustavo.trabalhofinal.ui.adapter

import android.content.Context
import android.content.SharedPreferences
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.gustavo.trabalhofinal.R
import com.gustavo.trabalhofinal.entities.Book
import kotlinx.android.synthetic.main.book_view.view.*

class BookAdapter(var books: List<Book>, private var listener: BookListener, private var user: Int) : RecyclerView.Adapter<BookAdapter.CategoriaViewHolder>() {
    lateinit var prefs: SharedPreferences

    override fun getItemCount() = books.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
            CategoriaViewHolder(
                    LayoutInflater
                            .from(parent.context)
                            .inflate(R.layout.book_view, parent, false)
            )

    override fun onBindViewHolder(holder: CategoriaViewHolder, position: Int) {
        holder.fillUI(books[position])
    }


    inner class CategoriaViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun fillUI(book: Book) {

            if(book.uri != null) {
                itemView.imgBook.setImageURI(Uri.parse(book.uri))
            }

            itemView.edit.visibility = View.GONE
            itemView.delete.visibility = View.GONE
            itemView.lblResume.visibility = View.GONE
            itemView.txtResume.visibility = View.GONE
            itemView.txtAuthor.visibility = View.GONE
            itemView.imgBook.visibility = View.VISIBLE
            itemView.txtTitle.text = book.title
            itemView.setOnClickListener {

                if (itemView.edit.visibility != View.VISIBLE || itemView.delete.visibility != View.VISIBLE || itemView.lblResume.visibility != View.VISIBLE || itemView.txtResume.visibility != View.VISIBLE || itemView.txtAuthor.visibility != View.VISIBLE) {
                    itemView.edit.visibility = View.VISIBLE
                    itemView.delete.visibility = View.VISIBLE
                    itemView.lblResume.visibility = View.VISIBLE
                    itemView.txtResume.visibility = View.VISIBLE
                    itemView.txtAuthor.visibility = View.VISIBLE
                    itemView.txtAuthor.text = book.author
                    itemView.txtResume.text = book.resume
                    itemView.lblResume.text = book.genre.toString()

                    itemView.delete.setOnClickListener{
                        if(book.id_user == user)
                            listener.excluirBook(book)
                    }
                    itemView.edit.setOnClickListener{

                        if(book.id_user == user)
                            listener.editarBook(book)
                    }
//                    Picasso
//                            .get()
//                            .load(article.urlToImage)
//                            .into(itemView.imgArticle)

                }else{
                    itemView.edit.visibility = View.GONE
                    itemView.delete.visibility = View.GONE
                    itemView.lblResume.visibility = View.GONE
                    itemView.txtResume.visibility = View.GONE
                    itemView.txtAuthor.visibility = View.GONE
                }
//                itemView.button.setOnClickListener {
//                    // cod massa listener.abrirActivity()
//                }


//
            }

        }


    }

}
