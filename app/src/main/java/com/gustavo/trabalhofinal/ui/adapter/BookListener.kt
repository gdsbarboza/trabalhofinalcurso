package com.gustavo.trabalhofinal.ui.adapter

import com.gustavo.trabalhofinal.entities.Book

interface BookListener {
    fun editarBook(book: Book)
    fun excluirBook(book: Book)
}