package com.gustavo.trabalhofinal.ui.adapter

import com.gustavo.trabalhofinal.entities.Lista

interface ListaListener {
    fun abrirListaSubitens(lista: Lista)
    fun deletarLista(lista: Lista)
}