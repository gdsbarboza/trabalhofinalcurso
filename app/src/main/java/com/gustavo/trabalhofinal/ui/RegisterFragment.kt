package com.gustavo.trabalhofinal.ui


import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.gustavo.trabalhofinal.R
import com.gustavo.trabalhofinal.entities.ReturnCreate
import com.gustavo.trabalhofinal.service.BancoService
import kotlinx.android.synthetic.main.fragment_register.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


/**
 * A simple [Fragment] subclass.
 */
class RegisterFragment : androidx.fragment.app.Fragment() {
    lateinit var retrofit: Retrofit
    lateinit var service: BancoService
    lateinit var prefs: SharedPreferences

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_register, container, false)
    }

    fun configuraRetrofit() {
        retrofit = Retrofit.Builder()
                .baseUrl("http://10.0.2.2/bd/")
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        service = retrofit.create(BancoService::class.java)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        prefs = this.activity!!.getSharedPreferences("prefs", Context.MODE_PRIVATE)

        submitButton.setOnClickListener {

            if (UserPassword.text.toString() == UserPassword1.text.toString()) {
                if (UserPassword.text.toString() != "" && Username.text.toString() != "" && UserPassword1.text.toString() != "" && UserEmail.text.toString() != "") {

                    var name = Username.text.toString()
                    var email = UserEmail.text.toString()
                    var password = UserPassword.text.toString()
                    configuraRetrofit()
                    service.createUser(name, email, password).enqueue(object : Callback<ReturnCreate> {
                        override fun onFailure(call: Call<ReturnCreate>, t: Throwable) {
                            Log.e("vick", t.message, t)
                        }

                        override fun onResponse(call: Call<ReturnCreate>, response: Response<ReturnCreate>) {
                            val r = response.body()!!


                            if (r.result) {
                                prefs.edit().putString("email", email).commit()
                                prefs.edit().putString("name", name).commit()
                                prefs.edit().putString("senha", password).commit()
                                findNavController().navigate(R.id.menu)
                            }
                        }

                    })
                } else {
                    Username.setText("")
                    UserEmail.setText("")
                    UserPassword.setText("")
                    UserPassword1.setText("")
                }

            }else {
                UserPassword.setText("")
                UserPassword1.setText("")
            }

        }


    }
}
