package com.gustavo.trabalhofinal.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.gustavo.trabalhofinal.R
import com.gustavo.trabalhofinal.entities.Option
import kotlinx.android.synthetic.main.card_note_modify_conclued.view.*

class SubitenListaAdapter(private var options: MutableList<Option>) :
        RecyclerView.Adapter<SubitenListaAdapter.SubitenListaViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SubitenListaViewHolder {
        val view = LayoutInflater
                .from(parent.context)
                .inflate(R.layout.card_note_modify_conclued, parent, false)
        return SubitenListaViewHolder(view)
    }

    override fun getItemCount() = options.size

    override fun onBindViewHolder(holder: SubitenListaViewHolder, position: Int) {
        val option = options[position]
        holder.preencherView(option)
    }

    inner class SubitenListaViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun preencherView(option: Option) {

            itemView.txtConclued.setText(option.option)
            itemView.setOnLongClickListener {
                options.remove(option)
                //val position = options.indexOf(option)
                //notifyItemRemoved(position)


            }
            itemView.setOnClickListener {
                itemView.txtConclued.isChecked = !itemView.txtConclued.isChecked
                val position = options.indexOf(option)
                notifyItemChanged(position)
            }
        }
    }


}