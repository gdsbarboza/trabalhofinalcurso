package com.gustavo.trabalhofinal.ui


import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SearchView
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.NavigationUI
import androidx.navigation.ui.setupWithNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.gustavo.trabalhofinal.R
import com.gustavo.trabalhofinal.entities.Book
import com.gustavo.trabalhofinal.entities.ReturnBooks
import com.gustavo.trabalhofinal.service.BancoService
import com.gustavo.trabalhofinal.ui.adapter.BookAdapter
import com.gustavo.trabalhofinal.ui.adapter.BookListener
import kotlinx.android.synthetic.main.fragment_search.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class SearchFragment : androidx.fragment.app.Fragment(), BookListener {
	lateinit var retrofit: Retrofit
	lateinit var service: BancoService
    lateinit var adapter: BookAdapter
	lateinit var prefs: SharedPreferences

	override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
	                          savedInstanceState: Bundle?): View? {
		// Inflate the layout for this fragment
		return inflater.inflate(R.layout.fragment_search, container, false)
	}
	fun configuraRetrofit() {
		retrofit = Retrofit.Builder()
				.baseUrl("http://10.0.2.2/bd/")
				.addConverterFactory(GsonConverterFactory.create())
				.build()
		service = retrofit.create(BancoService::class.java)
	}
    private fun loadRecyclerView(books: List<Book>) {

        if (listBooks != null) {

            val mRecycler = getView()!!.findViewById(R.id.listBooks) as RecyclerView
            mRecycler.layoutManager = LinearLayoutManager(getView()!!.context,RecyclerView.VERTICAL,false)
            adapter = BookAdapter(books,this,prefs.getInt("id_user", 0))
            mRecycler.adapter = adapter

        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

		prefs = this.context!!.getSharedPreferences("prefs", Context.MODE_PRIVATE)
		configuraRetrofit()
		super.onViewCreated(view, savedInstanceState)

		botton_navgint.setupWithNavController(
				Navigation.findNavController(view)
		)
		botton_navgint.setOnNavigationItemSelectedListener {
			NavigationUI.onNavDestinationSelected(it,
					Navigation.findNavController(view))
		}

		searchView.setOnQueryTextListener(object: SearchView.OnQueryTextListener{
			override fun onQueryTextSubmit(query: String?): Boolean {
                service.searchBook(query.toString()).enqueue(object : Callback<ReturnBooks> {
					override fun onFailure(call: Call<ReturnBooks>, t: Throwable) {
						Log.d("vick", t.message, t)
					}

					override fun onResponse(call: Call<ReturnBooks>, response: Response<ReturnBooks>) {

						val r = response.body()!!

						loadRecyclerView(r.books)

					}
				})

				return true
			}

			override fun onQueryTextChange(newText: String?): Boolean {

				return true
			}
		})
	}

	override fun editarBook(book: Book) {

		prefs.edit().putInt("id_book", book.id).commit()
		findNavController().navigate(R.id.add_book)
	}

	override fun excluirBook(book: Book) {
		TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
	}
}
