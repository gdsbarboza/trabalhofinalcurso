package com.gustavo.trabalhofinal.ui

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import androidx.navigation.ui.NavigationUI
import androidx.navigation.ui.setupWithNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.gustavo.trabalhofinal.R
import com.gustavo.trabalhofinal.entities.*
import com.gustavo.trabalhofinal.service.BancoService
import com.gustavo.trabalhofinal.ui.adapter.organizationAdapter
import kotlinx.android.synthetic.main.fragment_main.*
import kotlinx.android.synthetic.main.fragment_organization.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class OrganizationFragment : androidx.fragment.app.Fragment(){
	lateinit var retrofit: Retrofit
	lateinit var service: BancoService
	lateinit var adapter: organizationAdapter
	lateinit var prefs: SharedPreferences

	override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
							  savedInstanceState: Bundle?): View? {
		// Inflate the layout for this fragment
		return inflater.inflate(R.layout.fragment_organization, container, false)
	}

	private fun loadRecyclerView(books: List<Book>) {

		if (ListaViewHolder != null) {

			val mRecycler = getView()!!.findViewById(R.id.ListaViewHolder) as RecyclerView
			mRecycler.layoutManager = LinearLayoutManager(getView()!!.context, RecyclerView.VERTICAL, false)
			adapter = organizationAdapter(books)
			mRecycler.adapter = adapter
		}
	}
	fun configuraRetrofit() {
		retrofit = Retrofit.Builder()
				.baseUrl("http://10.0.2.2/bd/")
				.addConverterFactory(GsonConverterFactory.create())
				.build()
		service = retrofit.create(BancoService::class.java)
	}
	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		super.onViewCreated(view, savedInstanceState)
		configuraRetrofit()
		prefs = this.activity!!.getSharedPreferences("prefs", Context.MODE_PRIVATE)


		service.organizar(prefs.getInt("id_user",0)).enqueue(object : Callback<ReturnBooks> {
			override fun onFailure(call: Call<ReturnBooks>, t: Throwable) {
				Log.d("vick", t.message, t)
			}

			override fun onResponse(call: Call<ReturnBooks>, response: Response<ReturnBooks>) {
				if(response.body()!= null) {
					loadRecyclerView(response.body()!!.books)
				}
			}
		})


		bottomNavigationView.setupWithNavController(
				Navigation.findNavController(view)
		)

		bottomNavigationView.setOnNavigationItemSelectedListener {
			NavigationUI.onNavDestinationSelected(it,
					Navigation.findNavController(view))
		}

	}
}

