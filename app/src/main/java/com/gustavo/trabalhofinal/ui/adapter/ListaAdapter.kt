package com.gustavo.trabalhofinal.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.gustavo.trabalhofinal.R
import com.gustavo.trabalhofinal.entities.Lista
import kotlinx.android.synthetic.main.card_note_close.view.*
import kotlinx.android.synthetic.main.card_note_open.view.*

class ListaAdapter (private var listas: MutableList<Lista>, private var email: String,
                    private var listener: ListaListener) :
        RecyclerView.Adapter<ListaAdapter.ListaViewHolder>() {

    var listaEditando: Lista? = null

    fun criarLista(): Int {
        val posicao = 0
        val lista = Lista("", email)
        listaEditando = lista
        listas.add(posicao, lista)
        notifyItemInserted(posicao)
        return posicao
    }

    override fun getItemViewType(position: Int) =
            if (listaEditando === listas[position])
                R.layout.card_note_open
            else
                R.layout.card_note_close

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
            ListaViewHolder(
                    LayoutInflater
                            .from(parent.context)
                            .inflate(viewType, parent, false)
            )

    override fun getItemCount() = listas.size

    override fun onBindViewHolder(holder: ListaViewHolder, position: Int) =
            holder.preencherView(listas[position])

    inner class ListaViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun preencherView(lista: Lista) {

            if (listaEditando === lista) {
                itemView.txtNote.setText(lista.titulo)

                itemView.setOnClickListener {
                    listaEditando = null
                    notifyItemChanged(position)
                }

                itemView.btSalvar.setOnClickListener {
                    lista.titulo = itemView.txtNote.text.toString()

                    listaEditando = null
                    val position = listas.indexOf(lista)
                    notifyItemChanged(position)
                    listener.abrirListaSubitens(lista)
                }
                itemView.btApagar.setOnClickListener {


                    listaEditando = null
                    val position = listas.indexOf(lista)
                    notifyItemRemoved(position)
                    listas.remove(lista)
                    listener.deletarLista(lista)
                }
            } else {
                itemView.lblNote.text = lista.titulo

                itemView.setOnClickListener {
                    listaEditando = lista
                    val position = listas.indexOf(lista)
                    notifyItemChanged(position)
                }
            }

        }
    }
}