package com.gustavo.trabalhofinal.ui.adapter

import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.gustavo.trabalhofinal.R
import com.gustavo.trabalhofinal.entities.Book
import kotlinx.android.synthetic.main.book_view.view.*
import kotlinx.android.synthetic.main.card_livros_organizados.view.*

class organizationAdapter(var books: List<Book> ) : RecyclerView.Adapter<organizationAdapter.CategoriaViewHolder>(){
    override fun getItemCount() = books.size
    var aux: Int = 1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
            CategoriaViewHolder(
                    LayoutInflater
                            .from(parent.context)
                            .inflate(R.layout.card_livros_organizados, parent, false)
            )

    override fun onBindViewHolder(holder: CategoriaViewHolder, position: Int) {
        holder.fillUI(books[position])
    }


    inner class CategoriaViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun fillUI(book: Book) {
            itemView.local_book.text = aux.toString()
            itemView.book_text.text = book.title
            aux++
        }


    }



}