package com.gustavo.trabalhofinal.ui

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.common.collect.Lists
import com.gustavo.trabalhofinal.R
import com.gustavo.trabalhofinal.entities.Lista
import com.gustavo.trabalhofinal.entities.ReturnCreate
import com.gustavo.trabalhofinal.entities.ReturnListas
import com.gustavo.trabalhofinal.service.BancoService
import com.gustavo.trabalhofinal.ui.adapter.ListaAdapter
import com.gustavo.trabalhofinal.ui.adapter.ListaListener
import kotlinx.android.synthetic.main.fragment_list_add.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class ListAddFragment : Fragment(), ListaListener {


    lateinit var retrofit: Retrofit
    lateinit var service: BancoService
    lateinit var prefs: SharedPreferences
    lateinit var adapter: ListaAdapter
    lateinit var email: String
    lateinit var listasAux: List<Lista>
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_list_add, container, false)
    }
    fun configuraRetrofit() {
        retrofit = Retrofit.Builder()
                .baseUrl("http://10.0.2.2/bd/")
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        service = retrofit.create(BancoService::class.java)
    }
    fun configuraRecyclerView(email: String, listas: List<Lista>) {



        adapter = ListaAdapter(listas.toMutableList(), email, this)

        mrvList.adapter = adapter

        mrvList.layoutManager = LinearLayoutManager(getView()!!.context, RecyclerView.VERTICAL, false)
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        prefs = this.context!!.getSharedPreferences("prefs", Context.MODE_PRIVATE)
        email = prefs.getString("email","")


        configuraRetrofit()
        service.listLista(email).enqueue(object : Callback<ReturnListas> {
            override fun onFailure(call: Call<ReturnListas>, t: Throwable) {
                Log.d("vick", t.message, t)
            }

            override fun onResponse(call: Call<ReturnListas>, response: Response<ReturnListas>) {

               val r = response.body()!!
               for (list in r.listas){
                   if(list.usuario != email){
                       r.listas.drop(list.id)
                   }
               }

                listasAux = r.listas
                configuraRecyclerView(email,r.listas)
            }
        })

        btnPlus.setOnClickListener {
            val posicao = adapter.criarLista()
            mrvList.scrollToPosition(posicao)
        }
    }

    override fun abrirListaSubitens(lista: Lista) {
        prefs.edit().putString("listaTitle", lista.titulo).commit()

        if(listasAux.contains(lista)){
            prefs.edit().putInt("listaId", lista.id).commit()
            findNavController().navigate(R.id.action_listAddFragment_to_noteList)
        }else {
            service.createLista(lista.titulo, email).enqueue(object : Callback<Int> {
                override fun onFailure(call: Call<Int>, t: Throwable) {
                    Log.d("vick", t.message, t)
                }

                override fun onResponse(call: Call<Int>, response: Response<Int>) {

                    val r = response.body()!!


                    prefs.edit().putInt("listaId", r).commit()
                    findNavController().navigate(R.id.action_listAddFragment_to_noteList)

                }
            })
        }

    }
    fun deletarSubitens(id: Int){
        service.deleteOption(id).enqueue(object : Callback<ReturnCreate> {
            override fun onFailure(call: Call<ReturnCreate>, t: Throwable) {
                Log.d("vick", t.message, t)
            }

            override fun onResponse(call: Call<ReturnCreate>, response: Response<ReturnCreate>) {

                val r = response.body()!!

            }
        })
    }
    override fun deletarLista(lista: Lista) {
        var num = listasAux.indexOf(lista)
        deletarSubitens(listasAux[num].id)
        service.deleteLista(lista.titulo,email).enqueue(object : Callback<ReturnCreate> {
            override fun onFailure(call: Call<ReturnCreate>, t: Throwable) {
                Log.d("vick", t.message, t)
            }

            override fun onResponse(call: Call<ReturnCreate>, response: Response<ReturnCreate>) {

                val r = response.body()!!

            }
        })
    }
}
