package com.gustavo.trabalhofinal.ui


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import com.gustavo.trabalhofinal.R
import kotlinx.android.synthetic.main.fragment_initial_screen.*


/**
 * A simple [Fragment] subclass.
 */
class InitialScreenFragment : androidx.fragment.app.Fragment(){
	override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
							  savedInstanceState: Bundle?): View? {
		// Inflate the layout for this fragment
		return inflater.inflate(R.layout.fragment_initial_screen, container, false)
	}

	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		super.onViewCreated(view, savedInstanceState)

		logButton.setOnClickListener(Navigation.createNavigateOnClickListener(R.id.logar))
		registerButton.setOnClickListener(Navigation.createNavigateOnClickListener(R.id.registrar))

	}
}
