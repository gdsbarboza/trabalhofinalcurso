package com.gustavo.trabalhofinal.service

import com.gustavo.trabalhofinal.entities.*
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*

interface BancoService {
    @Headers("Accept: application/json")
    @GET("criar_usuario.php")
    fun createUser(

            @Query("name")
            name: String,

            @Query("email")
            email: String,

            @Query("password")
            password: String
    ): Call<ReturnCreate>

    @Headers("Accept: application/json")
    @GET("login_usuario.php")
    fun loginUser(

            @Query("email")
            email: String,

            @Query("password")
            password: String

    ): Call<User>


    @Headers("Accept: application/json")
    @GET("criar_livro.php")
    fun createBook(

            @Query("title")
            title: String,

            @Query("author")
            author: String,

            @Query("opinion")
            resume: String,

            @Query("genre_literary")
            genre: Int,

            @Query("type")
            type: Boolean,

            @Query("readed")
            readed: Boolean,

            @Query("url")
            uri: String,

            @Query("ano")
            ano: Int,

            @Query("edicao")
            edicao: Int,

            @Query("id_user")
            id_user: Int,

            @Query("lendable")
            lendable: Boolean
    ): Call<ReturnCreate>

    @Headers("Accept: application/json")
    @GET("buscar_livro.php")
    fun searchBook(

            @Query("string")
            string: String

    ): Call<ReturnBooks>

    @Headers("Accept: application/json")
    @GET("listar_livro_por_user.php")
    fun listOwnBooks(

            @Query("id")
            id: Int

    ): Call<ReturnBooks>

    @Headers("Accept: application/json")
    @GET("deletar_livro.php")
    fun deleteBook(

            @Query("id")
            id: Int

    ): Call<ReturnCreate>

    @Headers("Accept: application/json")
    @GET("pegar_id_user.php")
    fun searchIdUser(

            @Query("email")
            email: String,

            @Query("password")
            password: String

    ): Call<Int>

    @Multipart
    @POST("upload")
    fun upload(
            @Part("description") description: RequestBody,
            @Part file: MultipartBody.Part
    ): Call<okhttp3.MultipartBody>

    @Headers("Accept: application/json")
    @GET("listar_livro_digital.php")
    fun listDigitalBook(): Call<ReturnBooks>

//    @Headers("Accept: application/json")
//    @GET("listar_livro_fisico.php")
//    fun listPhysicalBook(): Call<ReturnBooks>

    @Headers("Accept: application/json")
    @GET("listar_Generos.php")
    fun listGenre(): Call<ReturnLiteraryGenre>

    @Headers("Accept: application/json")
    @GET("criar_lista.php")
    fun createLista(

            @Query("name")
            name: String,
            @Query("email_user")
            email_user: String

    ): Call<Int>

    @Headers("Accept: application/json")
    @GET("listar_listas.php")
    fun listLista(
            @Query("email_user")
                email: String

    ): Call<ReturnListas>

    @Headers("Accept: application/json")
    @GET("deletar_lista.php")
    fun deleteLista(

            @Query("name")
            name: String,
            @Query("email_user")
            email_user: String

    ): Call<ReturnCreate>

    @Headers("Accept: application/json")
    @GET("deletar_options_por_lista.php")
    fun deleteOption(

            @Query("list_id")
            list_id: Int

    ): Call<ReturnCreate>

    @Headers("Accept: application/json")
    @GET("criar_options.php")
    fun createOptions(

            @Query("list_id")
            list_id: Int,
            @Query("option")
            option: String,
            @Query("conclued")
            conclued: Boolean

    ): Call<ReturnCreate>

    @Headers("Accept: application/json")
    @GET("listar_options_por_lista.php")
    fun listOptions(

            @Query("list_id")
            list_id: Int

    ): Call<List<String>>

    @Headers("Accept: application/json")
    @GET("update_livro.php")
    fun updateBook(
            @Query("id")
            id: Int,

            @Query("title")
            title: String,

            @Query("author")
            author: String,

            @Query("opinion")
            resume: String,

            @Query("genre_literary")
            genre: Int,

            @Query("type")
            type: Boolean,

            @Query("readed")
            readed: Boolean,

            @Query("url")
            uri: String,

            @Query("ano")
            ano: Int,

            @Query("edicao")
            edicao: Int,

            @Query("id_user")
            id_user: Int,

            @Query("lendable")
            lendable: Boolean

    ): Call<ReturnCreate>


    @Headers("Accept: application/json")
    @GET("buscar_por_id_livros.php")
    fun searchBookId(
            @Query("id")
            id: Int

    ): Call<ReturnBooks>

    @Headers("Accept: application/json")
    @GET("organizar_livro.php")
    fun organizar(
            @Query("id")
            id: Int

    ): Call<ReturnBooks>

    @Headers("Accept: application/json")
    @GET("listar_livros_compartilhados.php")
    fun list_shared_books(): Call<List<RetornoShared>>

}

