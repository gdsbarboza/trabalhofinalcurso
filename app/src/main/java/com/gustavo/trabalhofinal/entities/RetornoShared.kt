package com.gustavo.trabalhofinal.entities


data class RetornoShared (
        var title: String,
        var author: String,
        var name: String,
        var email: String
)