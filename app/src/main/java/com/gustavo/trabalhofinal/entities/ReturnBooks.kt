package com.gustavo.trabalhofinal.entities


data class ReturnBooks(
        var books: List<Book>
)
