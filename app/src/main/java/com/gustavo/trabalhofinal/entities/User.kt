package com.gustavo.trabalhofinal.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.io.Serializable

@Entity(tableName = "User")
data class User (
        @SerializedName("name")
        var name: String,
        @SerializedName("password")
        var password: String

//json type convert ouu criiar dois do pergunta, um pra local e outro pra remoto

) : Serializable {
    @PrimaryKey()
    var email: String = ""
}