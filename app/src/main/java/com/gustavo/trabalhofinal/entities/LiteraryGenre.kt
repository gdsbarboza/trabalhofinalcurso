package com.gustavo.trabalhofinal.entities


import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.io.Serializable



@Entity(tableName = "genre_literary")
data class LiteraryGenre (
        @SerializedName("categoria")
        var categoria: String,
        @SerializedName("valor")
        var valor: Int

//json type convert ouu criiar dois do pergunta, um pra local e outro pra remoto

) : Serializable {
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0


}


