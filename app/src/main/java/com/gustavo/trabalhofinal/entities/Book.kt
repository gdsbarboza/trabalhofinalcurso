package com.gustavo.trabalhofinal.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.io.Serializable



@Entity(tableName = "Book")
data class Book (
        @SerializedName("title")
        var title: String,
        @SerializedName("genre_literary")
        var genre: Int,
        @SerializedName("opinion")
        var resume: String,
        @SerializedName("author")
        var author: String,
        @SerializedName("type")
        var type: Boolean,
        @SerializedName("readed")
        var readed: Boolean,
        @SerializedName("url")
        var uri: String,
        @SerializedName("ano")
        var ano: Int,
        @SerializedName("edicao")
        var edicao: Int,
        @SerializedName("id_user")
        var id_user: Int,
        @SerializedName("lendable")
        var lendable: Boolean


//json type convert ouu criiar dois do pergunta, um pra local e outro pra remoto


) : Serializable {
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0


}


