package com.gustavo.trabalhofinal.entities

data class ReturnLiteraryGenre (
        var lista: List<LiteraryGenre>
)