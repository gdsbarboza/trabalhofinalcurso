package com.gustavo.trabalhofinal.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "general_list")
data class Lista (
    @SerializedName("name")
    var titulo: String,
    @SerializedName("email_user")
    var usuario: String
){ @PrimaryKey(autoGenerate = true)
var id: Int = 0

val get_titulo get() = "$titulo"

override fun toString() = get_titulo
}