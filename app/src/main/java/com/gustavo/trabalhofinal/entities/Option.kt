package com.gustavo.trabalhofinal.entities

import androidx.room.PrimaryKey
import java.io.Serializable


data class Option (
        var list_id: Int,
        var option: String
)