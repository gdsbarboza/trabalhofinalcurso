package com.gustavo.trabalhofinal.entities

data class ReturnListas (
        var listas: List<Lista>
)